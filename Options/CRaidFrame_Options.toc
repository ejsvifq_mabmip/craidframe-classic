## Interface: 11302
## Author: cqwrteur
## Title: CRaidFrame Options
## Version: @project-version@
## Dependencies: CRaidFrame
## LoadOnDemand: 1
## X-CRF-MESSAGE: CRF_ChatCommand

#@no-lib-strip@
Libs\AceDBOptions-3.0\AceDBOptions-3.0.xml
Libs\AceLocale-3.0\AceLocale-3.0.xml
Libs\AceGUI-3.0\AceGUI-3.0.xml
Libs\AceGUI-3.0-SharedMediaWidgets\widget.xml
Libs\AceConfig-3.0\AceConfig-3.0.xml
#@end-no-lib-strip@
locale\locale.xml
Core.lua